import './App.css';
import { useState } from 'react';
import Button from './components/Button/Button.jsx';
import Modal from './components/Modal/Modal';
const actionStyle={
  backgroundColor:"rgba(0,0,0,0.3)",
  width:"100px",
  padding:"15px 25px",
  border:"none",
  borderRadius: "3px",
  color:"white",
  fontSize:"15px"
}
const actionDivStyle={
  display:"flex",
  gap:"10px",
  justifyContent:"center"
}


function App() {
  const [firstModal,firstModalState]=useState(false);
  const [secondModal,secondModalState]=useState(false);

  function changeState(modal){
    if(modal==="firstModal"){
      secondModalState(false);
      if(firstModal){
        firstModalState(false)
      }else{
        firstModalState(true);
      }
    }else{
      firstModalState(false)
      if(secondModal){
        secondModalState(false)
      }else{
        secondModalState(true);
      }
    }

  }
  return (
  <>
    <Button modal="firstModal" changeState={changeState} backgroundColor="red">Open first modal</Button>
    <Button modal="secondModal" changeState={changeState} backgroundColor="purple">Open second modal</Button>
    <Modal modal="firstModal" header="Do you want to delete this file?" closeButton={true} text="Onse you delete thi file? it won`t be possible to undo this action. Are you sure you want to delete it?" actions={
    <div style={actionDivStyle}>
      <button style={actionStyle}>Ok</button>
      <button style={actionStyle}>Cancel</button>
    </div>} isOpen={firstModal} changeState={changeState}/>
    <Modal  modal="secondModal" header="Hello!!!" closeButton={true} text="How are you?" actions={
    <div style={actionDivStyle}>
      <button style={actionStyle}>Fine</button>
      <button style={actionStyle}>Bad</button>
    </div>} isOpen={secondModal} changeState={changeState}/>
  </>
  );
}

export default App;
