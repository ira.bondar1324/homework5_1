import React from "react";
import styles from "./Modal.module.scss"


const Modal = ({modal,header,closeButton,text,actions,isOpen,changeState}) =>{

    let displayIs;
    isOpen?displayIs={display:"flex"}:displayIs={display:"none"};

    const click = (e)=>{
        if(e.target.className==="Modal_modalWrapper__2ZYE5"||e.target.className==="Modal_modalWindowHeaderCross__0CP-9"){
            changeState(modal);
        }
    }

    return(
        <div onClick={click} style={displayIs} className={styles.modalWrapper}>
            <div className={styles.modalWindow}>
                <header className={styles.modalWindowHeader}>
                    <h2 className={styles.modalWindowHeaderText}>{header}</h2>
                    {closeButton? <p className={styles.modalWindowHeaderCross}>X</p>:null}
                </header>
                <main className={styles.modalWindowMain}>
                    <p className={styles.modalWindowMainText}>{text}</p>
                    {actions}
                </main>
            </div>
        </div>
    );

}
export default Modal;