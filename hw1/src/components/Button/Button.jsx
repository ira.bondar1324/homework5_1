import React from "react";
import styles from "./Button.module.scss"

const Button = ({children,backgroundColor,changeState,modal}) => {
return(
    <button onClick={()=>{changeState(modal)}} className={styles.button} style={backgroundColor={backgroundColor}}>{children}</button>
);
}
export default Button;